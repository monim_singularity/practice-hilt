package com.singularitybd.learnhilt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.singularitybd.learnhilt.databinding.ActivityMainBinding
import com.singularitybd.learnhilt.view_models.implementation.TestViewModelImpl
import com.singularitybd.learnhilt.view_models.interfaces.TestViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

//    @Inject private lateinit var viewModel: TestViewModelImpl
    private val viewModel: TestViewModelImpl by viewModels()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        setContentView(binding.root)

        viewModel.mutableText.observe(this) { data ->
            print("=======>>>> $data")
        }
    }
}