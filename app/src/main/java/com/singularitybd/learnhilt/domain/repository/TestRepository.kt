package com.singularitybd.learnhilt.domain.repository

import android.view.View
import javax.inject.Singleton

interface TestRepository {
    suspend fun doSomeOtherTest()
    fun justTesting(view: View, text: String)
}