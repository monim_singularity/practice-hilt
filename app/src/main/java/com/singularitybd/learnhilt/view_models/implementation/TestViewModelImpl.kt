package com.singularitybd.learnhilt.view_models.implementation

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.singularitybd.learnhilt.domain.repository.TestRepository
import com.singularitybd.learnhilt.view_models.interfaces.TestViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import javax.inject.Singleton

@HiltViewModel
class TestViewModelImpl @Inject constructor(
    private val repository: TestRepository
): ViewModel(), TestViewModel {

    override var mutableText: MutableLiveData<String> = MutableLiveData()

    override fun justTesting(view: View) {
        repository.justTesting(view, mutableText.value ?: "")
    }

}