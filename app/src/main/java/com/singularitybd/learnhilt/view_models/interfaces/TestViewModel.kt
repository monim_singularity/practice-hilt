package com.singularitybd.learnhilt.view_models.interfaces

import android.view.View
import androidx.lifecycle.MutableLiveData

interface TestViewModel {
    var mutableText: MutableLiveData<String>
    fun justTesting(view: View)
}