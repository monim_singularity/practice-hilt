package com.singularitybd.learnhilt.data.repository

import android.app.Application
import android.view.View
import com.singularitybd.learnhilt.R
import com.singularitybd.learnhilt.data.remote.TestApi
import com.singularitybd.learnhilt.domain.repository.TestRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TestRepositoryImpl @Inject constructor(
    private val testApi: TestApi,
    private val context: Application
): TestRepository {

    init {
        println("This is test")
    }

    override suspend fun doSomeOtherTest() {
    }

    override fun justTesting(view: View, text: String) {
        println("((((((((((((((((=== $text")
    }
}