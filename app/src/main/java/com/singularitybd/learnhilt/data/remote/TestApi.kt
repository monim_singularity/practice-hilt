package com.singularitybd.learnhilt.data.remote

import retrofit2.http.GET
import javax.inject.Singleton

interface TestApi {

    @GET("test")
    suspend fun doSomeWork()

}