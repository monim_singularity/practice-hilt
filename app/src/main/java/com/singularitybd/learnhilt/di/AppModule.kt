package com.singularitybd.learnhilt.di

import android.app.Application
import com.singularitybd.learnhilt.data.remote.TestApi
import com.singularitybd.learnhilt.data.repository.TestRepositoryImpl
import com.singularitybd.learnhilt.domain.repository.TestRepository
import com.singularitybd.learnhilt.view_models.implementation.TestViewModelImpl
import com.singularitybd.learnhilt.view_models.interfaces.TestViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideTestApi(): TestApi {
        return Retrofit.Builder().baseUrl("https://google.com").build().create(TestApi::class.java)
    }

    @Provides
    fun provideTestRepository(api: TestApi, context: Application): TestRepository {
        return TestRepositoryImpl(api, context)
    }

}